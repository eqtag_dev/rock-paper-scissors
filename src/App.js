import React, {Component} from 'react';
import './App.css';

const symbols = [{id: 0, value: 'rock', loser: 'scissors'}, {id: 1, value: 'paper', loser: 'rock'}, {id: 2, value: 'scissors', loser: 'paper'}]
var chosenValue = symbols[0].value

const UserChoice  = () => {
    const selectChoices = symbols.map((item) =>
        <option key={item.id}>{item.value}</option>
    )

    return  (
      <div className="card">
          User choice
           <select onChange={(e) => chosenValue = e.target.value}>
               {selectChoices}
           </select>
      </div>
  )
}

const ComputerCard = ({symbol}) => {
  const style = {
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundImage: 'url(' + symbol + '.png)',
  };
  return (
      <div className="computer-card" style={style}>
          Computer
      </div>
  )
}

class App extends Component {
  constructor(props) {
      super(props);
      this.state = {}
  }

  findWinner = (player, computer)=> {
    if (player === computer) {
        return "Draw!"
    }
    let symbol = symbols.filter(function (symbol) {
        return symbol.value == player
    })

    if (symbol[0].loser === computer) {
        return "You won!"
    }  else {
        return "Sorry, computer won!"
    }
  }

  run = () => {
      const computerChoise = symbols[Math.floor(Math.random() * 3)].value
      console.log(computerChoise)
      console.log(chosenValue)

      this.setState({
          player: chosenValue ,
          computer: computerChoise,
          winner: this.findWinner(chosenValue, computerChoise)
      })
  }

  render() {
    return (
        <div className="App">
          <UserChoice />
          <ComputerCard symbol={this.state.computer} />
          <button onClick={this.run}>Play</button>
          <p>{this.state.winner}</p>
        </div>
    )
  }
}

export default App;















